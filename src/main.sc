require: slotfilling/slotFilling.sc
  module = sys.zb-common
  

    
theme: /

    state: Start
        q!: $regex</start>
        intent!: /играть
        a: Выберите игру
        buttons:
            "0-10" -> ./Launch10
            "Быки и коровы" -> ./LaunchCow
            
        state: Launch10
            go!: /Start10
        state: LaunchCow
            go!: /StartCow
        
    state: Start10
        a: Начнём игру. Бот загадал число от 0 до 10.
        go!: /Play
        
    state: Play
        script:
            $session.gameId = 0;
            $session.number = $jsapi.random(10) + 1;
    
    state: CheckNum
        intent!: /циферки
        script:
            var num = $parseTree._циферкаSlot;
            var nc = $session.numberCow;
            
            var k = 0
            
            $session.cows = 0;
            $session.bulls = 0;
            $session.debugmsg = String(num).charAt(0);
            
            if ($session.gameId == 0) {
                if (num == $session.number)
                    $reactions.answer("Вы выиграли! Загаданное число: {{$session.number}}. Напишите \"играть\" чтобы начать новую игру");
                    //$reactions.buttons({text: "Еще раз", transition: '/Start10'});
                    //$reactions.buttons({text: "Выбрать другую игру", transition: '/Start'});
                if (num < $session.number)
                    $reactions.answer("Загаданное число больше чем ваше");
                if (num > $session.number)
                    $reactions.answer("Загаданное число меньше чем ваше");
            }
            else
            {
                for (var i = 0; i < 4; i++) {
                    for (var j = 0; j < 4; j++) {
                        if (String(num).charAt(i) == String(nc).charAt(j) && i == j) {
                            $session.bulls = $session.bulls + 1;
                            j = 4;
                        }
                        else if (String(num).charAt(i) == String(nc).charAt(j))
                        {
                            $session.cows = $session.cows + 1;
                            j = 4;
                        }
                    }
                }
                if ($session.bulls == 4) {
                    $reactions.answer("Вы правильно угадали число: {{$session.numberCow}}. Напишите \"играть\" чтобы начать новую игру");
                }
                else {
                    $reactions.answer("Коровы: {{$session.cows}}, Быки: {{$session.bulls}}");
                }
                
                //$reactions.answer("коровы: {{$session.cows}}, быки: {{$session.bulls}}. debug {{$session.numberCow}}");
                //$reactions.answer("Это игра про корову. {{$session.numberCow}}");
            }
        
    state: StartCow
        intent!: /корова
        a: Начнем игру. Бот загадал четырехзначное число.
        go!: /PlayCow
        
    state: PlayCow
        script:
            $session.gameId = 1;
            //$session.numberCow = Math.floor(Math.random() * (10000 - 1000)) + 1000;
            
            var xd = "";
            var list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
            for (var i = 0; i < 4; i++) {
                var a = $jsapi.random(list.length-1);
                xd = xd + list[a];
                list.splice(a, 1)
            }
            $session.numberCow = Number(xd)
        
    
    

    state: Hello
        intent!: /привет
        a: Привет привет

    state: Bye
        intent!: /пока
        a: Пока пока
        

    state: NoMatch
        event!: noMatch
        a: Я не понял. Вы сказали: {{$request.query}}

